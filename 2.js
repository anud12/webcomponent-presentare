
const log = s => console.log("person-list", s);
//Register custom element
customElements.define("person-list", class extends HTMLElement {
    elements = {}
    listeners = {}

    static get observedAttributes() {
        return ['data'];
    }

    connectedCallback() {
        log("Connected")
        //Seteaza ca vrem shadowDom visibil si din exterior
        // (RTM https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM)
        const wrapper = this.attachShadow({mode: "open"})
        .appendChild(document.createElement("div"));
        const css = wrapper.appendChild(document.createElement("link"));
        css.rel = "stylesheet"
        css.href= "https://code.getmdl.io/1.3.0/material.indigo-pink.min.css"
        //Creeaza elementele si salveazale pentru cleanup
        this.elements.button = wrapper.appendChild(document.createElement("button"));
        this.elements.button.innerText = "Add";
        //Add event listener
        this.elements.button.addEventListener("click", this.onAdd)
        this.elements.button.setAttribute("class", "mdl-button mdl-js-button mdl-button--raised")

        const link = wrapper.appendChild(document.createElement("a"));
        link.setAttribute("href", "https://www.google.com")
        link.innerText = "This looks different"
        this.elements.data = wrapper.appendChild(document.createElement("pre"));
        //Read "props"
        this.elements.data.innerText = this.getAttribute("data");
    }

    disconnectedCallback() {
        log("Disconnected")
        //Clean event listener
        this.elements.button.removeEventListener("click", this.#onAdd)
    }

    attributeChangedCallback(name, oldValue, newValue) {
        //On prop change
        if(!this.elements.data) {
            return;
        }
        this.elements.data.innerText = newValue;
    }

    //Variabila privata ca sa nu fie rescrisa
    #onAdd = e => {
        e.preventDefault();
        this.listeners?.onAdd();
    }

})