import * as ReactDOM from "react-dom";
import * as React from "react";
import {Fragment, useState} from "react";

const log = s => console.log("person-form", s)
customElements.define("person-form", class extends HTMLElement {
    wrapper: HTMLElement;
    formState = {};

    constructor() {
        super();
    }

    connectedCallback() {
        log("Connected")
        this.wrapper = this.attachShadow({mode: "open"})
            .appendChild(document.createElement("div"))
        ReactDOM.render(
            <this.Render onChange={e => this.formState = e}/>,
            this.wrapper
        )
    }

    disconnectedCallback() {
        log("Disconnected")
        ReactDOM.unmountComponentAtNode(this.wrapper)
    }

    Render = (props) => {
        const [use, set] = useState<any>({})
        const on = name => e => {
            e.preventDefault();
            set(p => {
                const newP = {
                    ...p,
                    [name]: e.target.value
                }
                props.onChange(newP)
                return newP;
            })
        }
        return <Fragment>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/4.6.1/css/bootstrap-grid.min.css"/>
            <form>
                <label style={{display:"block"}}>
                    First name
                    <input className="ml-2 form-control" value={use?.firstName} onChange={on("firstName")}/>
                </label>
                <label style={{display:"block"}}>
                    Last name
                    <input className="ml-2 form-control" value={use?.lastName} onChange={on("lastName")}/>
                </label>
                <label style={{display:"block"}}>
                    Age
                    <input className="ml-2 form-control" value={use?.age} onChange={on("age")}/>
                </label>
            </form>
        </Fragment>
    }

})