import "https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js";

const log = s => console.log("person-listing", s)
customElements.define("person-listing", class extends HTMLElement {
    static get observedAttributes() {
        return ['firstname', "lastname", "age"];
    }
    constructor() {
        super();
    }
    app;
    connectedCallback() {
        log("Connected")
        this.wrapper = this.attachShadow({mode: "open"})
        .appendChild(document.createElement("div"))

        this.app = new Vue({
            el: this.wrapper,
            template: `<ul>
                            <li>First name : {{firstname}}</li>
                            <li>Last name: {{lastname}}</li>
                            <li>Age: {{age}}</li>
                        </ul>`,
            data: {
                firstname: this.getAttribute("firstname"),
                lastname: this.getAttribute("lastname"),
                age: this.getAttribute("age"),
            }
        })
    }

    disconnectedCallback() {
        log("Disconnected")
        this.app?.$destroy();
    }
    attributeChangedCallback(name, oldValue, newValue) {
        //On prop change
        if(!this.app) {
            return;
        }
        this.app[name] = newValue
    }

})